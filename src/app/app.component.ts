import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  todo = [
    {
      name: "Fraise",
      state: 0
    },
    {
      name: "Pomme",
      state: 0
    },
    {
      name: "Chips",
      state: 0
    },
    {
      name: "Lait",
      state: 0
    },

  ];
  ajout = '';
  addItem() {
    this.todo.push({
      name: this.ajout,
      state: 0
    })
    this.ajout = ""
  }
  removeItem(item) {
    var index = this.todo.indexOf(item);
    this.todo.splice(index, 1);
  }
  updateItem(item) {
    item.name = prompt('Nom Modifié:', item.name)
  }


}
